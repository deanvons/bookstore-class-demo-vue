const BOOKS = [
    {
        "bookId": 1,
        "isbN10": "0006479898",
        "isbN13": "9780006479895",
        "title": "A Clash of Kings",
        "genre": "Fantasy",
        "publisher": "HarperCollins Publishers",
        "releaseDate": "1999-10-01T00:00:00",
        "format": "Paperback",
        "language": "English",
        "length": 928,
        "description": "The second volume of A Song of Ice and Fire, the greatest fantasy epic of the modern age.",
        "coverImg": "https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/lrg/9780/0064/9780006479895.jpg",
        "qtyOnHand": 10,
        "authors": [{ "authorId": 1, "name": "George R. R. Martin" }]
    },
    {
        "bookId": 2, "isbN10": "0006486126", "isbN13": "9780006486121", "title": "A Feast for Crows", "genre": "Fantasy", "publisher": "HarperCollins Publishers", "releaseDate": "2011-08-22T00:00:00", "format": "Paperback", "language": "English", "length": 864, "description": "HBO's hit series A GAME OF THRONES is based on George R. R. Martin's internationally bestselling series A SONG OF ICE AND FIRE, the greatest fantasy epic of the modern age. A FEAST FOR CROWS is the fourth volume in the series.", "coverImg": "https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/lrg/9780/0064/9780006486121.jpg", "qtyOnHand": 10, "authors": [{ "authorId": 1, "name": "George R. R. Martin" }]
    },
    {
        "bookId": 3, "isbN10": "055357342X", "isbN13": "9780553573428", "title": "A Storm Of Swords", "genre": "Fantasy", "publisher": "HarperCollins Publishers", "releaseDate": "2005-09-15T00:00:00", "format": "Paperback", "language": "English", "length": 1216, "description": "Here is the third volume in George R. R. Martin's magnificent cycle of novels that includes A Game of Thrones and A Clash of Kings.", "coverImg": "https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/lrg/9780/5535/9780553573428.jpg", "qtyOnHand": 10, "authors": [{ "authorId": 1, "name": "George R. R. Martin" }]
    },
    {
        "bookId": 4, "isbN10": "0261102389", "isbN13": "9780261102385", "title": "The Lord of the Rings : Boxed Set", "genre": "Fantasy", "publisher": "HarperCollins Publishers", "releaseDate": "2008-04-01T00:00:00", "format": "Paperback", "language": "English", "length": 1216, "description": "Continuing the story of The Hobbit, this three-volume boxed set of Tolkien's epic masterpiece, The Lord of the Rings, features striking black covers based on Tolkien's own design, the definitive text, and three maps including a detailed map of Middle-earth.", "coverImg": "https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/lrg/9780/2611/9780261102385.jpg", "qtyOnHand": 5, "authors": [{ "authorId": 2, "name": "J. R. R. Tolkien" }]
    },
    {
        "bookId": 5, "isbN10": "0544445783", "isbN13": "9780544445789", "title": "The Hobbit and the Lord of the Rings", "genre": "Fantasy", "publisher": "HOUGHTON MIFFLIN", "releaseDate": "2014-10-21T00:00:00", "format": "Paperback", "language": "English", "length": 1504, "description": "This four-volume, deluxe pocket boxed set contains J.R.R. Tolkien's epic masterworks The Hobbit and the three volumes of The Lord of the Rings (The Fellowship of the Ring, The Two Towers, and The Return of the King).", "coverImg": "https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/lrg/9780/5444/9780544445789.jpg", "qtyOnHand": 2, "authors": [{ "authorId": 2, "name": "J. R. R. Tolkien" }]
    },
    {
        "bookId": 6, "isbN10": "0307743659", "isbN13": "9780307743657", "title": "The Shining", "genre": "Horror", "publisher": "Random House USA Inc", "releaseDate": "2020-02-07T00:00:00", "format": "Paperback", "language": "English", "length": 659, "description": "Before Doctor Sleep, there was The Shining, a classic of modern American horror from the undisputed master, Stephen King.", "coverImg": "https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/lrg/9780/3077/9780307743657.jpg", "qtyOnHand": 20, "authors": [{ "authorId": 3, "name": "Stephen King" }]
    }
]

export default BOOKS
