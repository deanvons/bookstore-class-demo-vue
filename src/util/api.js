const BOOK_API_URL = 'https://localhost:44304/api/Books'

export const getAllBooks = () => {
    return fetch(BOOK_API_URL)
        .then(response => response.json())
        .then(response => response)
}

export const getBookById = (bookId) => {
    return fetch(`${BOOK_API_URL}/${bookId}`)
        .then(response => response.json())
        .then(response => response)
}

export const getRandomUser = () => {
    return fetch('https://randomuser.me/api')
        .then(response => response.json())
        .then(response => response)
}