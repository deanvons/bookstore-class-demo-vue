import VueX from 'vuex'
import Vue from "vue"
import { getRandomUser } from './api'

Vue.use(VueX)

const store = new VueX.Store(
    {
        state: {
            user: { loaded: false }
        },
        mutations: {
            setUser: (state, payload) => {
                state.user = payload
            }
        },
        actions: {
            //get users from random user api
            async loadUser({ commit }) {
                const userResult = await getRandomUser()
                commit('setUser', userResult)
            }

        }
    }
)

export default store;